//
//  history_graph.h
//  f_gnuplot
//
//  Created by Arnaud Blanchard on 13/12/2016.
//
//

#ifndef GRAPH_H
#define GRAPH_H


void create_poi_loop(unsigned char *image, int width, int height, uint16_t *coords, int points_nb);

#endif /* history_graph_h */
