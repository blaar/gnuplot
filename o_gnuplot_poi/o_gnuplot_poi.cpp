#include "blc_channel.h"
#include "blc_program.h"
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <sys/time.h>
#include "graph.h"


char const *with_option, *style_option;

int sx, sy;
int sampling_period;
int interactive_mode=0;
int final_columns_nb, final_rows_nb;
int offset=0;
int bands=1;
int dims_nb, *lengths;
double min, max;
enum {RUN, PAUSE};
int status=RUN;

void refresh_period_cb(char const*argument, void*){
	sampling_period=strtol(argument, NULL, 10)*1000;
}

int main(int argc, char *argv[]){
    blc_channel input_image, poi;
	char const *poi_name, *input_image_name;

	blc_program_set_description("Display an image with the point of interests");
	blc_program_add_parameter(&input_image_name, "blc_channel-in", 1, "image on which you want to show the points of interest", NULL);
    blc_program_add_parameter(&poi_name, "blc_channel-in", 1, "points of interest", NULL);

	//   blc_program_add_option(&size_str, 's', "size", "UI32[xUI32]", "size of the matrix", NULL);
	blc_program_init(&argc, &argv, blc_quit);
    blc_command_forward_blc_channels();


	input_image.open(input_image_name, BLC_CHANNEL_READ);
    poi.open(poi_name, BLC_CHANNEL_READ);

    blc_loop_try_add_waiting_semaphore(input_image.sem_new_data);
    blc_loop_try_add_posting_semaphore(input_image.sem_ack_data);
    blc_loop_try_add_waiting_semaphore(poi.sem_new_data);
    blc_loop_try_add_posting_semaphore(poi.sem_ack_data);
    
    create_poi_loop(input_image.uchars, input_image.dims[0].length, input_image.dims[1].length, poi.uints16, poi.dims[0].length);
    
	return EXIT_SUCCESS;
}







