Gnuplot
=======

Links
-----

- [gnuplot](http://www.gnuplot.info) Official website
- [gnuplotting](http://www.gnuplotting.org) Nice blogs with lot of original exemples
- [Impossible gnuplot graphs](http://www.phyast.pitt.edu/~zov1/gnuplot/html/intro.html) see specifically [stats function](http://www.phyast.pitt.edu/~zov1/gnuplot/patch/stats.html)  (syntax is deprecated but the principle is there)

- [not so Frequently Asked Questions](http://lowrank.net/gnuplot/index-e.html)
- [gnuplot surprising](http://gnuplot-surprising.blogspot.fr) A bit old (2012) but interesting
