#include "blc_program.h"
#include "graph.h"
#include <pthread.h>
#include <sys/time.h>
#include <unistd.h>

static void init_term(FILE *pipef, char const *title, char const* verbatim){
    fprintf(pipef, "set term qt 1 noraise\n"); //Keep focus on the calling terminal
    fprintf(pipef, "set title '%s'\n", title); //axis x en y  only
    fprintf(pipef, "set border 3\n"); //axis x en y  only
    fprintf(pipef, "set title font ',20'\n");
    fprintf(pipef, "set label font ',10'\n");
    fprintf(pipef, "set key font ',10'\n");
    fprintf(pipef, "set terminal qt noenhanced\n");//avoid interpretation of '_'
    fprintf(pipef, "set boxwidth 0.9 relative\n");
    fprintf(pipef, "set grid\n");
    fprintf(pipef, "set yrange [* : *] reverse\n");
    if (verbatim) fprintf(pipef, "%s\n", verbatim);
}

void create_poi_loop(unsigned char *image, int width, int height, uint16_t *coords, int points_nb){
    char command[LINE_MAX];
    FILE *pipef;
    int text_offset=0;
    
    SYSTEM_ERROR_CHECK(pipef=popen("gnuplot", "w"), NULL, "calling gnuplot");
    init_term(pipef, "image", "");
    fprintf(pipef, "set palette gray\n");
 //  fprintf(pipef, "set view 30,190\n");
    
    text_offset=snprintf(command, LINE_MAX, "plot '-' binary format='%%uchar' array=%dx%d title 'image' with image ", width, height);
    text_offset=snprintf(command+text_offset, LINE_MAX, ", '-'    binary format='%%uint16%%uint16' record=%d  using 1:2:($0) title 'points of interest' with linespoints palette ", points_nb);
    
    BLC_COMMAND_LOOP(0){
        fprintf(pipef, "%s\n", command);
        SYSTEM_ERROR_CHECK(fwrite(image, sizeof(uchar), width*height, pipef), -1, NULL);
        SYSTEM_ERROR_CHECK(fwrite(coords, sizeof(uint16_t), 2*points_nb, pipef), -1, NULL);
        SYSTEM_ERROR_CHECK(fflush(pipef), -1, NULL);
    }
    SYSTEM_ERROR_CHECK(fclose(pipef), -1, NULL);
}



