# Set the minimum version of cmake required to build this project
cmake_minimum_required(VERSION 2.6)

project(gnuplot)

subdirs(o_gnuplot)
subdirs(o_gnuplot_poi)


