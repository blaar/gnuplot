//
//  history_graph.h
//  f_gnuplot
//
//  Created by Arnaud Blanchard on 13/12/2016.
//
//

#ifndef GRAPH_H
#define GRAPH_H
#include "blc_channel.h"

extern char const *with_option, *style_option;

void init_term(FILE *pipef, char const *title, char const* verbatim);
void create_history_graph(blc_channel *input, char const *title, int history_length, int refresh_period, int sampling_period, float min, float max, char const *verbatim);
void create_graph(blc_channel *input, char const *title, int refresh_period, float min, float max, float xmin, float xmax, float label_max, char const *verbatim);

#endif /* history_graph_h */
